import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';

import { RootComponent } from './root/root.component';
import { HomeComponent } from './home/home.component';
import { ServicesComponent } from './services/services.component';
import { ProjectsComponent } from './projects/projects.component';
import { CareersComponent } from './careers/careers.component';

export const AppRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'services', component: ServicesComponent },
    { path: 'portfolio', component: ProjectsComponent},
    { path: 'careers', component: CareersComponent}
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
