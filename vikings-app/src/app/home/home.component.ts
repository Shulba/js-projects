import { Component, OnInit } from '@angular/core';


declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  mainTitle: string = 'Conquer your market with us';
  context: any = this;
  constructor() {
   }

    scrollParalaxEvent(className, scrollEvent) {
        $(className).css({'background-position-y' : `${scrollEvent * (-0.43)}px`});
    }
  ngOnInit() {
    const context = this.context;
    $(window).scroll(function() {
      var wScroll = $(this).scrollTop();
      $('.media').css({
        'transform': `translate(0px, ${wScroll * 0.62}px)`, 'z-index': 5
      });
      context.scrollParalaxEvent( '.jumbotron', wScroll);
      context.scrollParalaxEvent( '.section-backgound-picture-1', wScroll);
      context.scrollParalaxEvent( '.section-backgound-picture-2', wScroll);
    });
    $('.single-items-wrapper').mousemove(function(e){
      let xDirection = e.screenX;
      let yDirection = e.screenY;
      //console.log(`X: ${xDirection} Y: ${yDirection}`);
      $(this).css({'background-position-x': `${(xDirection/5-55)*(-1)}px`, 'background-position-y': `${(yDirection/5-20)*(-1)}px`});
    });

    $('.single-items-wrapper').mouseout(function(){
      $(this).css({'background-position-x': `${0}px`, 'background-position-y': `${0}px`});
    });
  }

}
