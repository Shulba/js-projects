import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { ROUTING } from './app.routing';
import { ScrollEventModule } from 'ngx-scroll-event';

import { RootComponent } from './root/root.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { ProjectsComponent } from './projects/projects.component';
import { ServicesComponent } from './services/services.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CareersComponent } from './careers/careers.component';

@NgModule({
  declarations: [
    RootComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    ProjectsComponent,
    ServicesComponent,
    ContactUsComponent,
    CareersComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ROUTING,
    MatCheckboxModule,
    ScrollEventModule
  ],
  providers: [],
  bootstrap: [RootComponent]
})
export class AppModule { }
