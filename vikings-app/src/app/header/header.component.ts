import { Component, OnInit } from '@angular/core';
import { ScrollEvent } from 'ngx-scroll-event';
import { Router } from '@angular/router';
import { ActivatedRoute, Params, UrlSegment } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  whiteTheme = {
    backgroundNavbarClass: 'navbar navbar-expand-lg navbar-light show-menu-background',
    logotype: '../../assets/img/logo-vs-dark.svg',
    buttonClass: 'nav-link green-button'
  };
  darkTheme = {
    backgroundNavbarClass: 'navbar navbar-expand-lg navbar-dark hide-menu-background',
    logotype: '../../assets/img/logo-vs-white.svg',
    buttonClass: 'nav-link white-button'
  };
  showstate = this.darkTheme.backgroundNavbarClass;
  logotype = this.darkTheme.logotype;
  buttonClass = this.darkTheme.buttonClass;

  public href: string = '';
  public articleId;
  public secondRouter;
  public subscription;

  public homeRout;
  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,

    ) {
      const nextThis = this;

      setTimeout(function() {
        nextThis.href = nextThis.router.url;
        nextThis.figureLink(nextThis);
        if (nextThis.href === '/') {
          nextThis.menuFadeOut();
        } else {
          nextThis.menuFadeIn();
        }
      }, 500);
    }
  public figureLink(obj) {
    if (obj.href[1] === '#') {
      obj.href = '/';
      return obj.href;
    }
  }
  public menuFadeIn() {
      this.showstate = this.whiteTheme.backgroundNavbarClass;
      this.logotype = this.whiteTheme.logotype;
      this.buttonClass = this.whiteTheme.buttonClass;

  }
  public menuFadeOut() {
    this.showstate = this.darkTheme.backgroundNavbarClass;
    this.logotype = this.darkTheme.logotype;
    this.buttonClass = this.darkTheme.buttonClass;
  }
  public handleScroll(event: ScrollEvent) {

    this.href = this.router.url;
    this.figureLink(this);
    if (this.href === '/') {
      if (event.isReachingBottom) {
        this.menuFadeIn();
      }
      if (event.isReachingTop) {
        this.menuFadeOut();
      }
    } else {
      this.menuFadeIn();
    }
  }

  ngOnInit() {
  }
}
