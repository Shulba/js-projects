const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose');
const User = require('./model/user');
const NewsAPI = require('newsapi');
const app = express()
const url = 'mongodb://localhost/blogDb';
const newsapi = new NewsAPI('c45f7fda85bc43f98e7fdeb313606388');

newsapi.v2.everything({
  sources: 'bbc-news,the-verge'
}).then(response => {
  console.log(response);
  /*
    {
      status: "ok",
      articles: [...]
    }
  */
});

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended : false}))

app.post('/api/user/login', (req, res) => {
  mongoose.set('useCreateIndex', true);
  mongoose.connect(url,{ useNewUrlParser: true }, function(err){
      if(err) throw err;
      User.find({
          username : req.body.username, password : req.body.password
      }, function(err, user){
          if(err) throw err;

          if(user.length === 1){
              return res.status(200).json({
                  status: 'success',
                  data: user
              })
          } else {
              return res.status(200).json({
                  status: 'fail',
                  message: 'Login Failed'
              })
          }

      })
  });
})

app.post('/api/user/login', (req, res) => {
  mongoose.connect(url, function(err){
  if(err) throw err;
        console.log('connected successfully, username is ',req.body.username,' password is ',req.body.password);
    });
})


app.listen(3000, () => console.log('blog server running on port 3000!'))
