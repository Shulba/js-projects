			var signature_position_x = 0;
			var	signature_position_y = 0;
			var game_end_interval = 0;
			var game_end_status = false;
			var para = 0;
			var enemy = 0;
			var enemy_arr = [];    // називається масив
			var enemy_id = 0;
			var enemy_transfer = 0;
			var enemy_transfer_count = 0;
			var enemy_direction = 0;
			var number_of_enemy = Math.floor(Math.random()*5+1);   // визначається цілого рандомного числа від 1 - 5
			var enemy_bullet_id = 0;
			var enemy_bullet_arr = [];
			var enemy_bullet_transfer = 0;
			var enemy_transfer_for_bullets = 0;
			var enemy_bullet_random = 20;
			var enemy_shoot = Math.floor(Math.random()*enemy_bullet_random+1);
			var id_arr = [];
			var curent_id = 0;
			var enemy_time_count = 0;
			var high_items = 0;
			var bullet_interval = 0;
			var enemy_iterval = 0;
			var bullet_init = false;   // булеве значення яке еквівалентне 0
			var life_count=0;
			var score_count = 0;
			var transfer_items = 0;
			var score_block = "";
			var shipEscape = 0;
			var shipEscape_lose = 10;
			var level = 1;
			var level_score = 1200;
			var enemy_ship_delay = 30;
			var random_interval = Math.floor(Math.random()*50+10);
			var level_3_count = 0;
			var flame_count = 0;
			var flame_arr = [];		
			var boss_present = true;
			var boss_apear = false;
			var boss_life = Math.floor(Math.random()*100+1000);
			var boss_score = 0;
			var enemy_height = 70;
			var boss_level = 5;
			var time_to_new_type_emnemy = Math.floor(Math.random()*200+1);
			var main_sound  = 0;
			var expl_sound  = 0;
			var laser_sound = 0;
			var my_explosion = 0;
			var bos_sound = 0;
			window.onload=function(){   // скрипти які працюють після того як завантажуться після загрузки всього DOM дерева сайту (всіх об'єктів)
				//window.open('index.html', '_blank', 'resizable=no');
				var	banner = document.querySelector("#banner");
				var	glass = document.querySelector("#glass");
				var	textA = document.querySelector("#text_a");
				var	textB = document.querySelector("#text_b");
				var	textC = document.querySelector("#text_c");
				var	main_logo = document.querySelector("#main_logo");
				var spaceship_id = "";
				var defoult_position = false;
				var previos_move_position = 0;
				var rotate_state="top";
				var ship_rotate = 0;
				 main_sound= new Audio();
				function action_sound(){
					sound_stop()
					main_sound= new Audio();
					main_sound.src='Audio/03 Highway Star.mp3';
					main_sound.autoplay= true;
					main_sound.loop = true;
					main_sound.volume = 0.2;
				}
				function boss_sound(){
					sound_stop()
					main_sound= new Audio();
					main_sound.src='Audio/05 Peter Gunn Theme.mp3';
					main_sound.autoplay= true;
					main_sound.loop = true;
					main_sound.volume = 0.2;
				}
				function menu_sound(){
					main_sound= new Audio();
					main_sound.src='Audio/01 Born To Be Wild.mp3';
					main_sound.autoplay= true;
					main_sound.loop = true;
					main_sound.volume = 0.2;
				}
				function my_gun_sound(){
					laser_sound= new Audio();
					laser_sound.src='Audio/laser_sms.mp3';
					laser_sound.autoplay= true;
					laser_sound.loop = false;
					laser_sound.volume = 0.2;
				}
				function explosion_sound(){
					expl_sound= new Audio();
					expl_sound.src='Audio/Explosion+7.mp3';
					expl_sound.autoplay= true;
					expl_sound.loop = false;
					expl_sound.volume = 0.4;
				}
				function ship_explosion_sound(){
					my_explosion= new Audio();
					my_explosion.src='Audio/Explosion+8.mp3';
					my_explosion.autoplay= true;
					my_explosion.loop = false;
					my_explosion.volume = 0.4;
				}
				function sound_stop(){
					main_sound.pause();
					main_sound.currentTime = 0.0;
				}
			var direction ="center";
				banner.onmousemove = function(event){      // визначення положення мишки в пікселях на екрані
					previos_move_position = signature_position_y
					signature_position_x = event.clientX;  //  >
					signature_position_y = event.clientY;  // /\
					spaceship_id.style = "margin-left:"+signature_position_x+"px; margin-top:"+signature_position_y+"px;";
					/*if(previos_move_position<signature_position_y){
						rotate_state="bottom";
						//rotate_style()
					}else if(previos_move_position>signature_position_y){
						rotate_state="top";
						//
					}
					if(defoult_position==false){
						rotate_state="center";
					}*/
				}
				function ship_rotate_function(){
					if(rotate_state=="top"){
						spaceship_id.className = "space_ship_block ship_rotate_top";
					}else if(rotate_state=="bottom"){
						spaceship_id.className = "space_ship_block ship_rotate_bottom";
					}else if(rotate_state=="center"){
						rotate_style()
					}
				}
				function rotate_style(){
					defoult_position = true;
					ship_rotate = setTimeout(function(){
						spaceship_id.className = "space_ship_block";
						defoult_position = false
					}, 1000);
				}
				menu_sound()
				var life_wrapper = "";
				banner.onclick = function shootProcess(){          /* подія створення кулі при натискання на екрані */
					if(game_end_status == false){
						create_all_block("my-bullet", "myCoolElement_", curent_id);
						para.style = "margin-left:"+signature_position_x+"px; margin-top:"+(signature_position_y+23)+"px;"
						glass.appendChild(para);
						id_arr.push(curent_id);
						curent_id = curent_id + 1;    /* творюються об'єкти з ID селекторами по яких можна доступитись до створених нових об'єктів' */
						my_gun_sound()
						if(bullet_init == false){		/* створення об'єктів точок життя 3 штуки */
							action_sound()
								create_all_block("life_container", "life_conteiner_", 0);
								glass.appendChild(para);
								life_wrapper = document.getElementById("life_conteiner_0");
							while(life_count<3){
								life_count = life_count + 1;
								create_all_block("life", "life_", life_count);
								life_wrapper.appendChild(para);
								if(life_count == 3){  /* після того як створились показники життя створюються результати */
									create_all_block("score", "score_table", "");
									glass.appendChild(para);
									score_block = document.getElementById("score_table");
								}
							}
							textA.className = "none";  /* відключаються тексти і лого на банері */
							textB.className = "none";
							textC.className = "none";
							main_logo.className = "none";
							para = document.createElement("div");
							para.classList.add("glass");
							glass.appendChild(para);
							create_all_block("glass",NaN, NaN);
							create_all_block("space_ship_block", "main_ship_", 0);
							glass.appendChild(para);
							spaceship_id = document.getElementById("main_ship_0");
							level_set()
							start_shooting()
							bullet_init = true;
							enemy_create()
						}
					}
				}
				var time_to_shoot_counter = 0;
				function start_shooting(){			/* постійна функція яка викликається і спрацьовує 0.02 секунди */
					bullet_interval = setInterval(function(){
						enemy_shoot_process();
						//ship_rotate_function();
						bullet_move();
						enemy_move();
						score();
					},20);
				}
				function create_all_block(block_class, id_name_transfer, curent_id_transfer){
					para = document.createElement("div");
					para.classList.add(block_class);
					if(id_name_transfer){
						para.setAttribute('id',id_name_transfer+curent_id_transfer+"");
					}
				}
				function level_changes(level_transfer, banner_transfer, level_score_transfer, enemy_ship_delay_transfer){
					//continue_action()
					boss_disable()
					level = level_transfer;
					banner.className = banner_transfer;
					level_score = level_score_transfer;
					enemy_ship_delay = enemy_ship_delay_transfer;
					planet_apear = false;
					load_objects()
					planet_remove()
				}
				function flame_create(ship_pos_x, ship_pos_y, transfer_bullet, transfer_block){
					create_all_block("flame_block", "flame_", flame_count)
					para.style = "margin-left:"+(ship_pos_x-10)+"px; margin-top:"+(ship_pos_y-30)+"px;"
					glass.appendChild(para);
					if(transfer_bullet){
						glass.removeChild(transfer_bullet);
					}
					//if(transfer_block){
						time_delay = 2000;
						fire_erres( time_delay, flame_count)

				}
				function fire_erres( time_delay_transfer, flame_count_transfer){
					clear_fire=setTimeout(function(){
								var erese_block = flame_count_transfer;
								glass.removeChild(document.getElementById("flame_"+erese_block));
								//alert(erese_block)
					}, time_delay_transfer);
				}
				function style_move_calc(obj_var_name, obj_pos_x, obj_pos_y){
					obj_var_name.style = "margin-left:"+obj_pos_x+"px; margin-top:"+obj_pos_y+"px;";
				}
				function clear_block( obj_arr, obj_counter, obj_transfer){
					glass.removeChild(obj_transfer);
					obj_arr.splice(obj_counter,1);
				}
				var planet_apear = false;
				var planet_count = 0;
				var transfer_planet = "";
				function load_objects(){
					if((level==4)&&(planet_apear==false)){
						planet_apear = true;
						planet_count = 0;
						create_all_block("earth_view", "planet_", planet_count);
						glass.appendChild(para);
					}else if((level==6)&&(planet_apear==false)){
						planet_apear = true;
						planet_count = 1;
						create_all_block("mars_view", "planet_", planet_count);
						glass.appendChild(para);
					}
					if((level==8)&&(planet_apear==false)){
						planet_apear = true;
						planet_count = 2;
						create_all_block("saturn_view", "planet_", planet_count);
						glass.appendChild(para);
						transfer_planet = document.getElementById("planet_"+planet_count+"")
						create_all_block("planet_view", "planet_", planet_count+1);
						transfer_planet.appendChild(para);
					}
				}
				function planet_remove(){
					if((level!=4)&&(level!=6)&&(level!=8)){
						glass.removeChild(document.getElementById("planet_"+planet_count+""));	
					}
				}
				function add_boss_sound(){
					sound_stop()
					boss_sound()
					//action_sound()
				}
				function continue_action(){
					sound_stop()
					action_sound()
				}
				function score(){   /* відображення результатів */
					score_block.innerHTML = score_count+"/ "+level_score +" score " +level+" Level<br>"+ shipEscape+"/ "+shipEscape_lose+" spaceship you missed";
					if((level == 1)&&(score_count==0)){
						banner.className = "clear main-banner_start background_animation";
					}else if((score_count>level_score)&&(level== 1)){
						level_changes(2, "clear main-banner_a background_animation", 4500, 20);
					}else if((score_count>level_score)&&(level== 2)){
						level_changes(3, "clear main-banner_b background_animation", 9500, 20);
					}else if((score_count>level_score)&&(level== 3)){	
						level_changes(4, "clear main-banner_c space_background_animation", 17500, 10);
					}else if((score_count>level_score)&&(level== 4)){
						boss_life = Math.floor(Math.random()*100+1000);
						enemy_bullet_random = 10;
						add_boss_sound()
						level_changes(5, "clear main-banner_d background_animation", "level boss", 10);
					}else if((score_count>level_score)&&(level== 5)){	
						continue_action()	
						level_changes(6, "clear main-banner_e space_background_animation", 39500, 10);
					}else if((score_count>level_score)&&(level== 6)){	
						level_changes(7, "clear main-banner_f background_animation", (49500+ boss_score), 10);
					}else if((score_count>level_score)&&(level== 7)){
						level_changes(8, "clear main-banner_g space_background_animation", 59500, 10)
					}else if((score_count>level_score)&&(level== 8)){
						boss_life = Math.floor(Math.random()*200+2000);
						level_changes(9, "clear main-banner_h background_animation", (69500+ boss_score), 10);
					}else if((score_count>level_score)&&(level== 9)){
						boss_life = Math.floor(Math.random()*200+2000);
						level_score = level_score + boss_score;
						enemy_bullet_random = 7;
						add_boss_sound()
						level_changes(10, "clear main-banner_i background_animation", "Prime Boss", 0);
					}else if((score_count>level_score)&&(level== 10)){
						boss_life = Math.floor(Math.random()*200+2000);
						level_changes(11, "clear main-banner_j background_animation", "Prime Boss", 0);
					}
					
				}
				function bullet_move(){  /* прорахунок позиції кожної моєї кулі її напрямок */
					for(var bullet_count = 0; bullet_count<id_arr.length; bullet_count++){
						transfer_items = document.getElementById("myCoolElement_"+id_arr[bullet_count]+"");
						for(var enemy_count = 0; enemy_count<enemy_arr.length; enemy_count++){
							enemy_transfer = document.getElementById("enemyElement_"+enemy_arr[enemy_count]+"");
							 if((((transfer_items.offsetLeft+50)>=(enemy_transfer.offsetLeft))&&((transfer_items.offsetLeft)<=(enemy_transfer.offsetLeft)))&&(((transfer_items.offsetTop+4)>=(enemy_transfer.offsetTop))&&((transfer_items.offsetTop)<=(enemy_transfer.offsetTop+enemy_height)))){   // умова якщо куля потрапляє в корабель то він вибухає	
								flame_create(enemy_transfer.offsetLeft, transfer_items.offsetTop, NaN, enemy_transfer_block);

								if((level==10)||(level==5)){
									boss_life = boss_life - 10;
									boss_score = boss_score + 50;
								}
								if((level!=boss_level)||((level==boss_level)&&(boss_life<=0))){
									if(level==boss_level){
										boss_present = false;
									}
									if(enemy_transfer.className == "enemy_block_1"){
										enemy_transfer.className = "enemy_block_1_hit_1";
									}else if(enemy_transfer.className == "enemy_block_1_hit_1"){
										enemy_transfer.className = "enemy_block_1_hit_2";
									}else{
										clear_block(enemy_arr, enemy_count, enemy_transfer);
									}
									if(level == 5){
										var clear_fire=setTimeout(function(){
											level_score = 17500 + boss_score;
											score_count = level_score + 50;						
											enemy_bullet_random = 10;
											enemy_ship_delay = 20;
											boss_life = Math.floor(Math.random()*1000+1000);
											enemy_height = 70;
											boss_level = 10;
											standsrt_boss_params()
										}, 2000);
									}
									if((level==10)&&(boss_life<=0)){
										game_end_interval = setTimeout(function(){
											game_eress()
										}, 3000);
									}	
								}
								explosion_sound()
								score_count = score_count + 50;
								clear_block(id_arr, bullet_count, transfer_items);
								var enemy_transfer_block = document.getElementById("flame_"+flame_count+"");
								flame_count = flame_count +1;
							}
						}
						if(transfer_items.offsetLeft>1475){   // куля зникає в кінці екрану
								clear_block( id_arr, bullet_count, transfer_items)
							}
					}
				}
				var flame_anim= 0;
				var flame_interval = 0;
				var bullet_burst = 0;

				var bullet_boss_timeout = 0;
				var boss_time_count = 0;
				var boss_shoot_dilay = false;
				var life_block = "";
				function enemy_shoot_process(){
					enemy_shoot = enemy_shoot - 1;
					if((enemy_shoot<=0)&&(enemy_arr.length>0)){
						enemy_shoot = Math.floor(Math.random()*enemy_bullet_random+1);	
						enemy_transfer_for_bullets = document.getElementById("enemyElement_"+enemy_arr[Math.floor(Math.random()*enemy_arr.length+1)]+"");	
						if((level==10)||(level==5)){
						    time_to_shoot_counter = Math.floor(Math.random()*200+1);
							if((time_to_shoot_counter>150)&&(time_to_shoot_counter<160)){
								enemy_shoot = Math.floor(Math.random()*60+10);
							}
						}	
						create_all_block("enemy-bullets", "bullet_", enemy_bullet_id);
						if((level!=10)&&(level!=5)){
							style_move_calc(para, enemy_transfer_for_bullets.offsetLeft, (enemy_transfer_for_bullets.offsetTop+20))
						}else if((level==10)||(level==5)){
							enemy_transfer_for_bullets = document.getElementById("enemyElement_"+(enemy_id-1)+"");
							if(level==5){
								style_move_calc(para, enemy_transfer_for_bullets.offsetLeft, (enemy_transfer_for_bullets.offsetTop+Math.floor(Math.random()*170+1)))
							}else if(level==10){
								bullet_burst = Math.floor(Math.random()*6+1);
								if(bullet_burst <= 1){
									para.classList.add("enemy-bullets-top-direction");
								}else if((bullet_burst > 1)&&(bullet_burst < 6)){
									para.classList.add("enemy-bullets");
								}else if(bullet_burst >= 6){
									para.classList.add("enemy-bullets-bottom-direction");
								}
								style_move_calc(para, enemy_transfer_for_bullets.offsetLeft, (enemy_transfer_for_bullets.offsetTop+Math.floor(Math.random()*200+1)))
							}
						}
						glass.appendChild(para);
						enemy_bullet_arr.push(enemy_bullet_id)
						enemy_bullet_id = enemy_bullet_id+1;
					}
					for(var enemy_bullet_count = 0; enemy_bullet_count<enemy_bullet_arr.length; enemy_bullet_count++){
						enemy_bullet_transfer = document.getElementById("bullet_"+enemy_bullet_arr[enemy_bullet_count]+"");
						if(((signature_position_x+40)>(enemy_bullet_transfer.offsetLeft))&&((signature_position_x)<(enemy_bullet_transfer.offsetLeft+50))&&((signature_position_y+60)>(enemy_bullet_transfer.offsetTop))&&((signature_position_y)<(enemy_bullet_transfer.offsetTop+10))){
							 life_block = document.getElementById("life_"+life_count+"");
							
							life_wrapper.removeChild(life_block);			
							life_count = life_count - 1;	enemy_bullet_arr.splice(enemy_bullet_count,1);
							ship_explosion_sound()
							var enemy_transfer_block = document.getElementById("flame_"+flame_count+"");
							flame_create((signature_position_x+10), (signature_position_y+10), enemy_bullet_transfer, enemy_transfer_block);
							flame_count = flame_count +1;
								document.getElementById("glass").style="background:radial-gradient(#FFFFFF 60%,  #FFF694 80%); z-index:140;";
								var clear_fire=setTimeout(function(){
									document.getElementById("glass").style="";
								}, 100);
								fire_erres(enemy_transfer_block, 2000)
							if(life_count <= 0){
								game_eress()
							}
						}else if(shipEscape >= shipEscape_lose){
							game_eress()
						}else if(enemy_bullet_transfer.offsetLeft<5){
							clear_block( enemy_bullet_arr, enemy_bullet_count, enemy_bullet_transfer);
						}
					}
				}
				function game_eress(){
					game_end_status = true;
					textA.className = "block large-text";
					textB.className = "block small-text";
					textC.className = "block";
					main_logo.className = "block main-logo";
					if((boss_present == false)&&(level==10)){
						textA.innerHTML = "You";
						textB.innerHTML = "Win!!!";
						textC.innerHTML = "You score is: "+score_count+" points";
						glass.innerHTML = "";
					}else{
						textA.innerHTML = "Game";
						textB.innerHTML = "over";
						textC.innerHTML = "You lose!";
						glass.innerHTML = "";
						if(shipEscape >= shipEscape_lose){
							textC.innerHTML = "Too much enemy ships you missed";
						}
					}
								clearInterval(bullet_interval);
								clearInterval(enemy_iterval);
								clearInterval(game_cheat_reload);
								sound_stop()
								menu_sound()
								game_end_interval = setTimeout(function(){
									game_end_status = false;
								}, 10000);
								para = 0;
								 enemy = 0;
								 enemy_arr = [];
								 enemy_id = 0;
								 enemy_transfer = 0;
								 enemy_transfer_count = 0;
								 enemy_direction = 0;
								 number_of_enemy = Math.floor(Math.random()*5+1);
								 enemy_bullet_id = 0;
								 enemy_bullet_arr = [];
								 enemy_bullet_transfer = 0;
								 enemy_transfer_for_bullets = 0;
								 enemy_bullet_random = 20;
								 enemy_shoot = Math.floor(Math.random()*enemy_bullet_random+1);
								 id_arr = [];
								 curent_id = 0;
								 enemy_time_count = 0;
								 high_items = 0; 
								 enemy_ship_delay = 30;
								 random_interval = Math.floor(Math.random()*50+10);
								 bullet_interval = 0;
								 enemy_iterval = 0;
								 bullet_init = false;
								 life_count=0;
								 score_count = 0;
								 transfer_items = 0;
								 score_block = ""; 
								 shipEscape = 0;
								 shipEscape_lose = 10;
								 level = 1;
								 level_score = 1200;
								 level_3_count = 0;
								 boss_score = 0;
			 					flame_count = 0;
								flame_arr = [];
								boss_present = true;
								boss_directin = true;
								enemy_height = 70;
								boss_apear = false;
								boss_directin_y = true;
								boss_life = Math.floor(Math.random()*500+500);
								boss_level = 5;
								boss_aproach = false;
 								boss_aproach_time = 0;
								boss_present = true;

								 var restore_screen = setTimeout(function(){
								 	textA.innerHTML = "Space";
									textB.innerHTML = "Attack";
									textC.innerHTML = "Press to start";
									banner.className = "clear main-banner background_animation";
								 }, 5000); 
				}
				var life_present = false;
				var new_life_Block = "";
				var life_id = 0;
				var random_life_interval = 0;
				function create_life(){
					life_id = life_id + 1;
					life_present = true;
					create_all_block("extra_life", "new_life_", life_id);
					para.style = "margin-left:"+1150+"px; margin-top:"+Math.floor(Math.random()*480+1)+"px;"
					glass.appendChild(para);
					new_life_Block = document.getElementById("new_life_"+life_id +"");
				}
				function enemy_create(){
					enemy_iterval =  setInterval(function(){
						if(number_of_enemy == 0){
							enemy_time_count = enemy_time_count +1;	
						}else{
							random_interval = random_interval - 1;
						}
						if(enemy_time_count>random_interval){

							if((level>3)&&(level!=5)&&(level!=10)&&(life_present==false)&&(life_count<3)){
								random_life_interval = Math.floor(Math.random()*300+1);
								if((random_life_interval<270)&&(random_life_interval>250)){
									create_life();
								}
								
							}/**/
							var enemy_number = 0;
							if(level==1){
								var enemy_number = Math.floor(Math.random()*1+1);
							}else if(level==2){
								var enemy_number = Math.floor(Math.random()*2+1);
							}else if(level==3){
								var enemy_number = Math.floor(Math.random()*2+1);
							}else if(level==4){
								var enemy_number = Math.floor(Math.random()*2+2);
							}else if(level==6){
								var enemy_number = Math.floor(Math.random()*3+1);
							}else if((level>6)&&(level!=10)){
								var enemy_number = Math.floor(Math.random()*2+2);
							}else if((level == 10)&&(boss_present==true)&&(boss_apear == false)||(level == 5)&&(boss_present==true)&&(boss_apear == false)){
								if((level == 5)||(level == 10)){
									boss_level = level;
								}
									for(var enemy_count_a = 0; enemy_count_a<enemy_arr.length+1; enemy_count_a++){
										enemy_transfer = document.getElementById("enemyElement_"+enemy_arr[0]+"");
										glass.removeChild(enemy_transfer);
										enemy_arr.shift();
										enemy_count_a = 0;
									}
								if(level == 10){
									enemy_height = 200;
								}else if(level == 5){
									enemy_height = 170;
								}
								boss_apear = true;
								create_more_enemy();
							}
							if((level!=10)&&(level!=5)){
								enemy_height = 70;
								for(var bandit_number = 0; bandit_number<enemy_number; bandit_number++){
									create_more_enemy()
								}
							}
						}
					},100 ); 
				}

				function create_more_enemy(){
					number_of_enemy = number_of_enemy-1;
					enemy_time_count = 0;
							random_interval = Math.floor(Math.random()*enemy_ship_delay+2);
							enemy = document.createElement("div");
							if(level==10){
								enemy.classList.add("enemy_boss");
							}else if(level==5){
								enemy.classList.add("enemy_boss_first");
							}else
								if(level>=3){
									time_to_new_type_emnemy = Math.floor(Math.random()*200+1);
									if(((time_to_new_type_emnemy>155)&&(time_to_new_type_emnemy<157)&&(level==3))||((time_to_new_type_emnemy>155)&&(time_to_new_type_emnemy<158)&&(level==4))||((time_to_new_type_emnemy>155)&&(time_to_new_type_emnemy<160)&&(level==6))||((time_to_new_type_emnemy>150)&&(time_to_new_type_emnemy<165)&&(level>=7))||((time_to_new_type_emnemy>145)&&(time_to_new_type_emnemy<170)&&(level==9))){
										enemy.classList.add("enemy_block_1");
									}else {
										enemy.classList.add("enemy_block");
									}
								}else{
									enemy.classList.add("enemy_block");
								}	
							enemy.setAttribute('id',"enemyElement_"+enemy_id+"");
							enemy.style = "margin-left:"+(1450+Math.floor(Math.random()*200+1))+"px; margin-top:"+Math.floor(Math.random()*480+1)+"px;"
							glass.appendChild(enemy);
							enemy_arr.push(enemy_id);
							enemy_id = enemy_id+1;
				}
var boss_directin = true;
var boss_directin_y = true;
var boss_aproach = false;
var boss_aproach_time = 0;
				function enemy_move(){
					for(var enemy_count = 0; enemy_count<enemy_arr.length; enemy_count++){
						if((level!=10)&&(level!=5)){
							enemy_transfer = document.getElementById("enemyElement_"+enemy_arr[enemy_count]+"");
						}else{
							enemy_transfer = document.getElementById("enemyElement_"+enemy_arr[0]+"");
						}
						var enemy_direction = 0;
						var enemy_direction_x = 0;
						if((level!=10)&&(level!=5)){
							if(enemy_transfer.offsetTop==signature_position_y){
								enemy_direction = 0;
							}else if((enemy_transfer.offsetTop>signature_position_y)&&((enemy_transfer.offsetLeft-600)< signature_position_y)){
								enemy_direction = -2;
							}else if((enemy_transfer.offsetTop<signature_position_y)&&((enemy_transfer.offsetLeft-600)< signature_position_y)){
								enemy_direction = 2;
							}else{
								enemy_direction = 0;
							}
						}
						if((level==10)||(level==5)){
							if(boss_aproach == false){
								 if((enemy_transfer.offsetLeft>=1000)&&(boss_directin == true)){
									enemy_direction_x = -1;
									if(enemy_transfer.offsetLeft<1010){
										boss_directin = false;
									}
								}else if((enemy_transfer.offsetLeft<=1100)&&(boss_directin == false)){
									enemy_direction_x = 1;
									if(enemy_transfer.offsetLeft>1090){
										boss_directin = true;
									}
								}
								 if((enemy_transfer.offsetTop+100)>signature_position_y){
									enemy_direction = -2;
								}else if((enemy_transfer.offsetTop+100)<signature_position_y){
									enemy_direction = 2;
								}else{
									enemy_direction = 0;
								}
								if(level!=5){
									 clear_fire=setTimeout(function(){
										boss_aproach_time = Math.floor(Math.random()*2000+2);
										if((boss_aproach_time>500)&&(boss_aproach_time<535)){
											boss_aproach = true;
										}
									}, 4000);	
								}
							}else if((boss_aproach == true)&&(level==10)){
								if((enemy_transfer.offsetLeft>=90)&&(boss_directin == true)){
									enemy_direction_x = -8;
									if(enemy_transfer.offsetLeft<100){
										boss_directin = false;
									}
								}else if((enemy_transfer.offsetLeft<=1100)&&(boss_directin == false)){
									enemy_direction_x = 7;
									if(enemy_transfer.offsetLeft>1090){
										boss_aproach = false;
										boss_directin = true;
									}
								}
							}
							enemy_transfer.style = "margin-left:"+(enemy_transfer.offsetLeft+enemy_direction_x)+"px; margin-top:"+(enemy_transfer.offsetTop+enemy_direction)+"px;";
						}else{
							enemy_transfer.style = "margin-left:"+(enemy_transfer.offsetLeft-6)+"px; margin-top:"+(enemy_transfer.offsetTop+enemy_direction)+"px;";
						}
						enemy_transfer_count = enemy_count;
						if(((signature_position_x+20)>(enemy_transfer.offsetLeft))&&((signature_position_x)<(enemy_transfer.offsetLeft+50))&&((signature_position_y+50)>(enemy_transfer.offsetTop))&&((signature_position_y)<(enemy_transfer.offsetTop+70))){ 
								flame_create((signature_position_x+10), (signature_position_y-10), NaN, NaN);
								var life_block = document.getElementById("life_"+life_count+"");
								life_wrapper.removeChild(life_block);	
								life_count = life_count - 1;
								ship_explosion_sound()
								document.getElementById("glass").style="background:radial-gradient(#FFFFFF 60%,  #FFF694 80%);";
								var clear_fire=setTimeout(function(){
									document.getElementById("glass").style="";
								}, 100);
								if((level==10)||(level==5)){
									boss_life = boss_life - 10;
								}
								if(((level!=10)||((level==10)&&(boss_life<=0)))&&((level!=5)||((level==5)&&(boss_life<=0)))){
									clear_block( enemy_arr, enemy_count, enemy_transfer);
								}
								var enemy_transfer_block = document.getElementById("flame_"+flame_count+"");
								flame_create(enemy_transfer.offsetLeft, enemy_transfer.offsetTop, NaN, enemy_transfer_block);
								flame_count = flame_count + 1;
							if(life_count <= 0){
								game_eress()
							}
						}else if(enemy_transfer.offsetLeft<5){
							clear_block( enemy_arr, enemy_count, enemy_transfer);
							shipEscape = shipEscape + 1;
						}else if((life_count<3)&&((signature_position_x+30)>(new_life_Block.offsetLeft))&&((signature_position_x)<(new_life_Block.offsetLeft+30))&&((signature_position_y+50)>(new_life_Block.offsetTop))&&((signature_position_y)<(new_life_Block.offsetTop+30))){
								//alert("life")
								life_present=false;
								life_count = life_count + 1;
								create_all_block("life", "life_", life_count);
								life_wrapper.appendChild(para);
								glass.removeChild(document.getElementById("new_life_"+life_id));
						}

						if((new_life_Block.offsetLeft<0)&&(life_present)){
							//(level>3)&&(level!=5)&&(level!=10)&&(life_present==false)&&(life_count<3)
							life_present=false;
							glass.removeChild(document.getElementById("new_life_"+life_id));
						}else{
							
						}

					}
					
					return enemy_transfer;
				}
var cheat_arr = []; 
var game_cheat_reload;
var key_cheat = 0;
var cheat_time = 0;
var cheat_text = '';
var first_start = 10;
				function level_set(){
					game_cheat_reload = setInterval(function(){
						document.onkeydown = function(e){
							key_cheat = e.keyCode;
							cheat_time = 0;
							if(key_cheat==76){ //
								cheat_text = "l";
							}else if(key_cheat==50){
								cheat_text = "2";
							}else if(key_cheat==51){
								cheat_text = "3";
							}else if(key_cheat==52){
								cheat_text = "4";
							}else if(key_cheat==54){
								cheat_text = "6";
							}else if(key_cheat==55){
								cheat_text = "7";
							}else if(key_cheat==56){
								cheat_text = "8";
							}else if(key_cheat==57){
								cheat_text = "9";
							}else if(key_cheat==80){
								alert("Pause")
							}else if(key_cheat==8){
								cheat_arr[0] = '';
							}
							cheat_arr[0] = cheat_arr+cheat_text;
							if(cheat_arr[0]=="l2"){
								score_count = 3500;
								level_changes(2, "clear main-banner_a background_animation", 4500, 20);
							}	
							if(cheat_arr[0]=="l3"){
								score_count = 8500;
								level_changes(3, "clear main-banner_b background_animation", 9500, 20);
							}	
							if(cheat_arr[0]=="l4"){
								boss_life = Math.floor(Math.random()*100+1000);
								enemy_height = 170;		
								boss_level = 5;
								score_count = 16500;
								banner.className = "clear main-banner_c space_background_animation";
								level_changes(4, "clear main-banner_c space_background_animation", 17500, 10);
							}
							if(cheat_arr[0]=="l6"){
								score_count = 38500;
								level_changes(6, "clear main-banner_e space_background_animation", 39500, 10);
							}				
							if(cheat_arr[0]=="l7"){
								score_count = 48500;
								level_changes(7, "clear main-banner_f background_animation", 49500, 10);
							}
							if(cheat_arr[0]=="l8"){
								score_count = 58500;
								level_changes(8, "clear main-banner_g space_background_animation", 59500, 10);
							}
							if(cheat_arr[0]=="l9"){
								boss_life = Math.floor(Math.random()*200+2000);
								enemy_height = 200;		
								boss_level = 10;
								score_count = 68500;
								level_changes(9, "clear main-banner_h background_animation", 69500, 10);
							}

							load_objects()
						}
						if(cheat_arr[0]!=''){
							cheat_time = cheat_time+1;
						}
						if(cheat_time == 500){
								cheat_time = 0;
								cheat_arr[0] = '';
							} 
							first_start = 10000;
					}, first_start);
				}
				function boss_disable(){
					enemy_height = 170;
					standsrt_boss_params()
					cheat_arr[0] = '';
				}
				function standsrt_boss_params(){
					boss_present = true;
					boss_directin = true;
					boss_apear = false;
					boss_directin_y = true;
					boss_aproach = false;
 					boss_aproach_time = 0;										
				}
			}