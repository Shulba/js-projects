// User interface logic -------------------
$(window).ready(function(){
    let popup = $("#popup");
    let selectedItem = $("#selected-item");
        popup.hide();
    $("#main-form").submit(function(event){
        let firstName = $("input#new-first-name").val();
        let lastName = $("input#new-last-name").val();
        let phoneNumber = $("input#new-phone-number").val();
        let contactsSection = $("#contacts-section")

        if((firstName)&&(lastName)&&(phoneNumber)){
            let someContact = new Contacts(firstName, lastName, phoneNumber);
            boock.addContactsToBoock(someContact, firstName, lastName, phoneNumber, contactsSection, popup);
        }else{
            boock.popupForm(popup, `there is empty field`, true)
        }
        event.preventDefault();
    });
    $("#contacts-section" ).on("click", "button", function(){
        boock.deleteContact(this.value); 
        boock.popupForm(selectedItem, '', false);
        $(this).parent(".single-contact").remove();
    })
    $("#contacts-section" ).on("click", "div", function(){
        boock.popupForm(selectedItem, '', true);
        toggleStyle.call(this)
        selectedItem.html(boock.dispayInfo(2, $(this).children(".del-btn").val()));
    })
})
function toggleStyle(){
    $(".single-contact").removeClass("selected");
    $(this).addClass("selected");
}

// Business Logic for Contacts ---------
class Contacts{
    constructor(firstName, lastName, phone){
        this.firstName= firstName;
        this.lastName= lastName;
        this.phoneNumber = phone;
    }
}
//Business Logic for AddressBook ---------
class ContactBook{
   constructor(){
        this.boockId = 0;
        this.allContacts = [];
   }
    uniqueID(){
       return this.boockId +=1
    }
    addContactsToBoock(contacts, fName, lName, phoneNumber, contactsSection, popup){
        let contentState = true;
        for(var i of this.allContacts){
            if((i)&&(i.firstName === fName)&&(i.lastName === lName)){ 
                contentState = false;
                if(i.phoneNumber != phoneNumber){
                    contentState = true;
                }
            }
        }
        if(contentState){
            this.boockId = this.uniqueID();
            contacts.id = this.boockId;
            this.allContacts.push(contacts);
            contactsSection.append(this.dispayInfo(1));     // all at once  forEach
            this.popupForm(popup, ``, false)
        }else{
            this.popupForm(popup, `contact already exist`, true);
        }
    }
    findContact(id){
        for(let i in this.allContacts){
            if((this.allContacts[i].id)&&(this.allContacts[i].id === id)){
                return this.allContacts[i];
            }
        }return false
    }
    deleteContact(id){
        for(let i in this.allContacts){
            if((this.allContacts[i])&&(this.allContacts[i].id == id)){
                delete this.allContacts[i];
                return true;
            }
        }return false
    }
    dispayInfo(displayState, id){
       let lastContact;
       if(displayState===1){
            lastContact = this.findContact(this.allContacts.length);
           return `<div class="single-contact">First name: <span class="emphasis-words">${lastContact.firstName}</span>
           Last name: <span class="emphasis-words">${lastContact.lastName}</span> and Phone number: <span class="emphasis-words">${lastContact.phoneNumber}</span> 
           <button value="${lastContact.id}" class="del-btn" id="del-btn">Del</button></div>`
       }else if((displayState===2)&&(this.findContact(parseInt(id)))){
            lastContact = this.findContact(parseInt(id));
            return `<div class="chose-contact"><h2>Selected contact</h2><p>First name: <span class="emphasis-words">${lastContact.firstName}</p>
            </span><p> Last name: <span class="emphasis-words">${lastContact.lastName}</span></p>
            <p> Phone number: <span class="emphasis-words">${lastContact.phoneNumber}</span> </button></p></div>`
       }
    }
    popupForm(popup, text, popupState){
        popup.html(text);
        if(popupState){
            popup.show();
        }else{
            popup.hide();
        }
    }
}
var boock = new ContactBook();

