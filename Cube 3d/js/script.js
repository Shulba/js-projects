//console.log(1)
let rotateXdeg = 31;
let rotateYdeg = -3;
let rotateZdeg = 0;

let rotationSwitcher = false;
let keyboardActive = false;

window.onload = function(){
    //const rotationButton = document.querySelector("#main-btn");
    let rotationButton = document.querySelector("#main-btn");
    let mainBtnKeyboard = document.querySelector("#main-btn-keyboard");
    let cube = document.querySelector("#cube");
    let rotateInterval;

    rotate(rotateXdeg, rotateYdeg, rotateZdeg);
    rotateObjext();
    rotationButton.addEventListener("click", function(){
        rotateObjext();
    });
    mainBtnKeyboard.addEventListener("click", function(){
        if(keyboardActive==false){
            keyboardActive = true;
            rotationSwitcher= false;
            clearInterval(rotateInterval);
        }else{
            keyboardActive = false;
        }
    });
    function rotateObjext(){
        if(rotationSwitcher==false){
            rotationSwitcher = true;
            rotateInterval = setInterval(function(){
                rotateXdeg+=1;
                rotateYdeg+=1.5;
                rotateZdeg+=1;
                rotateCheck()
                cube.style = rotate(rotateXdeg, rotateYdeg, rotateZdeg);
            }, 10);
        }else{
            clearInterval(rotateInterval);
            rotationSwitcher = false;
        }
    }
    function rotateCheck(){
        if(rotateXdeg>360){
            if(rotateXdeg<1){
                rotateXdeg = 360;
            }else{
                rotateXdeg = 1;
            }
        }
        if(rotateYdeg>360){
            if(rotateYdeg<1){
                rotateYdeg = 360;
            }else{
                rotateYdeg = 1;
            }
        }
        if(rotateZdeg>360){
            if(rotateZdeg<1){
                rotateZdeg = 360;
            }else{
                rotateZdeg = 1;
            }
        }
    }
    function rotate(rotateXdeg, rotateYdeg, rotateZdeg){
        return  `transform: rotateY(${rotateYdeg}deg) rotateX(${rotateXdeg}deg) rotateZ(${rotateZdeg}deg)`
    }
    document.onkeydown = function(event){
        console.log(event.keyCode)
        if(keyboardActive){
            rotateCheck()
            if(event.keyCode == 38){
                rotateXdeg+=1;
            }else if(event.keyCode == 39){
                rotateYdeg+=1;
            }else if(event.keyCode == 40){
                rotateXdeg-=1;
            }else if(event.keyCode == 37){
                rotateYdeg-=1;
            }else if(event.keyCode == 97){
                rotateZdeg-=1;
            }else if(event.keyCode == 99){
                rotateZdeg+=1;
            }
            cube.style = rotate(rotateXdeg, rotateYdeg, rotateZdeg);
        }
    }
};

